/*
 * l3gd20.h
 *
 *  Created on: 13.03.2018
 *      Author: Piotr Gogola
 */

/**
 * \file l3gd20.h
 * \brief Header file with L3GD20 gyroscope driver.
 * Driver is written for STM32L476VGT6 microcontroller with HAL library.
 * Communication is realized with SPI 4-wire (full-duplex).
 */

#ifndef L3GD20_H_
#define L3GD20_H_

#include <stdint.h>
#include "../stm32l4xx_hal.h"
/** @addtogroup L3GD20
  * @{
  */

/**
 * \struct L3GD20_Rotation_t
 * \brief Structure which holds last read rotation values
 *  around 3 axis: X, Y, Z
 */
typedef struct {
	int16_t xAxisRotation;	//!< rotation around gyroscope local x axis
	int16_t yAxisRotation;	//!< rotation around gyroscope local y axis
	int16_t zAxisRotation; 	//!< rotation around gyroscope local z axis
} L3GD20_Rotation_t;

/**
 * \brief Initialize gyroscope
 * Write values into L3GD20 registers with SPI.
 */
void L3GD20_Init();

/**
 * \brief Set L3GD20 chip select pin in high state.
 */
void L3GD20_SelectChipHigh();

/**
 * \brief Set L3GD20 chip select pin in low state.
 */
void L3GD20_SelectChipLow();

/**
 * \brief Read 8-bit register
 * \param address register address to read value from
 * \return 8-bit value from register with address given by parameter
 */
uint8_t L3GD20_ReadReg8(uint8_t address);

/**
 * \brief Write 8-bit register
 * \param address	register address to write value in
 * \param data		data which will be written into register with address given by address parameter
 */
void L3GD20_WriteReg8(uint8_t address, uint8_t data);

/**
 * \brief Read 16-bit number saved in 2 registers
 * \param address lower register address to read value from
 * \return 16-bit value from register with address given by parameter
 */
uint16_t L3GD20_ReadReg16(uint8_t address);

/**
 * \brief Write 16-bit register
 * \param address	register address to write value in
 * \param data		data which will be written into registers with address given by address parameter
 */
void L3GD20_WriteReg16(uint8_t address, uint16_t data);

/**
 * \brief Read last updated rotation raw data from gyroscope
 * \param rotation pointer to L3GD20_Rotation_t structure
 */
void L3GD20_ReadRotation(L3GD20_Rotation_t* rotation);

/**
 * @}
 */

#endif /* L3GD20_H_ */
