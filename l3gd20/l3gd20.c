/*
 * l3gd20.c
 *
 *  Created on: 13.03.2018
 *      Author: Piotr Gogola
 */

/*!
 * \file l3gd20.c
 * \brief Definition of function for L3GD20 gyroscope
 */

#include "l3gd20.h"

#include "main.h"
/** @defgroup L3GD20 L3GD20
 * @{
 */

/*!
 * \brief Redeclaration of SPI2
 * Communication with gyroscope is happened with SPI interface
 */
extern SPI_HandleTypeDef hspi2;

/** @defgroup L3GD20_Constants L3GD20 Constants
 * @{
 */
#define WHO_AM_I	0x0F

#define CTRL_REG1	0x20
#define CTRL_REG2	0x21
#define CTRL_REG3	0x22
#define CTRL_REG4	0x23
#define CTRL_REG5	0x24

#define REFERENCE	0x25
#define OUT_TEMP	0x26
#define STATUS_REG	0x27

#define OUT_X_L		0x28
#define OUT_X_H		0x29
#define OUT_Y_L		0x2A
#define OUT_Y_H		0x2B
#define OUT_Z_L		0x2C
#define OUT_Z_H		0x2D

#define FIFO_CTRL_REG	0x2E
#define FIFO_SRC_REG	0x2F

#define INT1_CFG	0x30
#define INT1_SRC	0x31

#define INT1_THS_XH		0x32
#define INT1_THS_XL		0x33
#define INT1_THS_YH		0x34
#define INT1_THS_YL		0x35
#define INT1_THS_ZH		0x36
#define INT1_THS_ZL		0x37

#define INT1_DURATION	0x38

#define SINGLE_READ		0x80
#define MULTIPLE_READ	0xC0

#define SINGLE_WRITE	0x00
#define MULTIPLE_WRITE	0x40

/**
 * @}
 */

/** @defgroup L3GD20_Macros L3GD20 Macros
 * @{
 */
#define SBL(x)	(1<<x)
/**
 * @}
 */

/** @addtogroup L3GD20_Constants
 * @{
 */
/**
 * CTRL_REG1 bits
 */
#define DR1		SBL(7)
#define DR0 	SBL(6)
#define BW1		SBL(5)
#define BW0		SBL(4)
#define PD		SBL(3)
#define Zen		SBL(2)
#define Xen		SBL(1)
#define Yen		SBL(0)

/**
 * CTRL_REG2 bits
 */
#define HPM1 	SBL(5)
#define HPM0 	SBL(4)
#define HPCF3 	SBL(3)
#define HPCF2 	SBL(2)
#define HPCF1	SBL(1)
#define HPCF0	SBL(0)

/**
 * CTRL_REG3 bits
 */
#define I1_Int1 	SBL(7)
#define I1_Boot 	SBL(6)
#define H_Lactive 	SBL(5)
#define PP_OD 		SBL(4)
#define I2_DRDY 	SBL(3)
#define I2_WTM 		SBL(2)
#define I2_ORun 	SBL(1)
#define I2_Empty	SBL(0)

/**
 * CTRL_REG4 bits
 */
#define BDU		SBL(7)
#define BLE 	SBL(6)
#define FS1 	SBL(5)
#define FS0		SBL(4)
#define SIM		SBL(0)

/**
 * CTRL_REG5 bits
 */
#define BOOT 		SBL(7)
#define FIFO_EN 	SBL(6)
#define HPen 		SBL(4)
#define INT1_Sel1 	SBL(3)
#define INT1_Sel0 	SBL(2)
#define Out_Sel1 	SBL(1)
#define Out_Sel0	SBL(0)

/**
 * STATUS_REG bits
 */
#define ZYXOR 	SBL(7)
#define ZOR 	SBL(6)
#define YOR  	SBL(5)
#define XOR  	SBL(4)
#define ZYXDA  	SBL(3)
#define ZDA  	SBL(2)
#define YDA  	SBL(1)
#define XDA 	SBL(0)

/**
 * FIFO_CTRL_REG bits
 */
#define FM2		SBL(7)
#define FM1		SBL(6)
#define FM0		SBL(5)
#define WTM4	SBL(4)
#define WTM3	SBL(3)
#define WTM2	SBL(2)
#define WTM1	SBL(1)
#define WTM0	SBL(0)

/**
 * FIFO_SRC_REG bits
 */
#define WTM 	SBL(7)
#define OVRN 	SBL(6)
#define EMPTY 	SBL(5)
#define FSS4 	SBL(4)
#define FSS3	SBL(3)
#define FSS2 	SBL(2)
#define FSS1 	SBL(1)
#define FSS0	SBL(0)

/**
 * INT1_CFG bits
 */
#define AND_OR	SBL(7)
#define LIR		SBL(6)
#define ZHIE	SBL(5)
#define ZLIE	SBL(4)
#define YHIE	SBL(3)
#define YLIE	SBL(2)
#define XHIE	SBL(1)
#define XLIE	SBL(0)

/**
 * INT1_SRC bits
 */
#define IA		SBL(6)
#define ZH		SBL(5)
#define ZL		SBL(4)
#define YH		SBL(3)
#define YL		SBL(2)
#define XH		SBL(1)
#define XL		SBL(0)

/**
 * INT1_THS_XH bits
 */
#define THSX14 	SBL(6)
#define THSX13 	SBL(5)
#define THSX12 	SBL(4)
#define THSX11 	SBL(3)
#define THSX10  SBL(2)
#define THSX9 	SBL(1)
#define THSX8 	SBL(0)

/**
 * INT1_THS_XL bits
 */
#define THSX7 	SBL(7)
#define THSX6 	SBL(6)
#define THSX5 	SBL(5)
#define THSX4 	SBL(4)
#define THSX3 	SBL(3)
#define THSX2   SBL(2)
#define THSX1 	SBL(1)
#define THSX0 	SBL(0)

/**
 * INT1_THS_YH bits
 */
#define THSY14 	SBL(6)
#define THSY13 	SBL(5)
#define THSY12 	SBL(4)
#define THSY11 	SBL(3)
#define THSY10  SBL(2)
#define THSY9 	SBL(1)
#define THSY8 	SBL(0)

/**
 * INT1_THS_YL bits
 */
#define THSY7 	SBL(7)
#define THSY6 	SBL(6)
#define THSY5 	SBL(5)
#define THSY4 	SBL(4)
#define THSY3 	SBL(3)
#define THSY2   SBL(2)
#define THSY1 	SBL(1)
#define THSY0 	SBL(0)

/**
 * INT1_THS_ZH bits
 */
#define THSZ14 	SBL(6)
#define THSZ13 	SBL(5)
#define THSZ12 	SBL(4)
#define THSZ11 	SBL(3)
#define THSZ10  SBL(2)
#define THSZ9 	SBL(1)
#define THSZ8 	SBL(0)

/**
 * INT1_THS_ZL bits
 */
#define THSZ7 	SBL(7)
#define THSZ6 	SBL(6)
#define THSZ5 	SBL(5)
#define THSZ4 	SBL(4)
#define THSZ3 	SBL(3)
#define THSZ2   SBL(2)
#define THSZ1 	SBL(1)
#define THSZ0 	SBL(0)

/**
 * INT1_DURATION bits
 */
#define WAIT 	SBL(7)
#define D6 	SBL(6)
#define D5 	SBL(5)
#define D4 	SBL(4)
#define D3 	SBL(3)
#define D2  SBL(2)
#define D1 	SBL(1)
#define D0 	SBL(0)

/**
 * @}
 */

void L3GD20_Init() {
	/**
	 * Transmision off - chip select in high state
	 */
	L3GD20_SelectChipHigh();
	HAL_Delay(100);

	/**
	 * Enable interrupt on INT1 pin
	 * Interrupt when INT1 pin is low
	 */
	L3GD20_WriteReg8(CTRL_REG3, I1_Int1 | H_Lactive);

	/**
	 * Enable interrupts when rotation around Z-axis is above 5k
	 */
	L3GD20_WriteReg8(INT1_CFG, ZHIE);
	L3GD20_WriteReg8(INT1_THS_ZL, 0xff & 5000);
	L3GD20_WriteReg8(INT1_THS_ZH, 0xff & (5000 >> 8));

	/**
	 * Wait with generating an interrupt for 0.5sec
	 *
	 * WAIT bit = 1 	- If signal crosses the selected threshold, the interrupt
	 * falls after the duration has counted the number of samples in selected data rate
	 * written into the duration counter.
	 *
	 * Duration time depends on ODR
	 * ODR = 190Hz, set duration time 0.1sec requires about 20 samples
	 */
	L3GD20_WriteReg8(INT1_DURATION, WAIT | (20));

	/**
	 * Enable gyroscope
	 * Output data rate: 190Hz		- DR0
	 * Bandwidth, cut-off frequency: 50Hz		- BW1
	 * Measure rotation velocity around X, Y, Z axes	- Zen, Xen, Yen
	 */
	L3GD20_WriteReg8(CTRL_REG1, DR0 | BW1 | PD | Zen | Xen | Yen);
	HAL_Delay(200);
}

void L3GD20_SelectChipHigh() {
	HAL_GPIO_WritePin(MEMS_GYRO_CS_GPIO_Port, MEMS_GYRO_CS_Pin, GPIO_PIN_SET);
}

void L3GD20_SelectChipLow() {
	HAL_GPIO_WritePin(MEMS_GYRO_CS_GPIO_Port, MEMS_GYRO_CS_Pin, GPIO_PIN_RESET);
}

uint8_t L3GD20_ReadReg8(uint8_t address) {
	L3GD20_SelectChipLow();
	uint8_t receive = 0;
	address |= SINGLE_READ;
	/**
	 * Change mode into 2 full-duplex (2 lines needed)
	 */
	__SPI_DIRECTION_2LINES(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
	HAL_SPI_Receive(&hspi2, &receive, 1, 10);
	L3GD20_SelectChipHigh();
	return receive;
}

void L3GD20_WriteReg8(uint8_t address, uint8_t data) {
	L3GD20_SelectChipLow();
	address |= SINGLE_WRITE;
	/**
	 * Change mode into 2 full-duplex (2 lines needed)
	 */
	__SPI_DIRECTION_2LINES(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
	HAL_SPI_Transmit(&hspi2, &data, 1, 10);
	L3GD20_SelectChipHigh();
}

uint16_t L3GD20_ReadReg16(uint8_t address) {
	uint16_t receive = 0;
	L3GD20_SelectChipLow();
	address |= MULTIPLE_READ;
	/**
	 * Change mode into 2 full-duplex (2 lines needed)
	 */
	__SPI_DIRECTION_2LINES(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
	HAL_SPI_Receive(&hspi2, (uint8_t*) &receive, 2, 10);
	/**
	 * Swap bytes.
	 * It is used because registers with output value from
	 * sensor is ordered in Little Endian but INT1_THS registers are in Big Endian
	 * e.g.:
	 * OUT_X_L 0x28
	 * OUT_X_H 0x29
	 *
	 * INT1_THS_XH 0x32
	 * INT1_THS_XL 0x33
	 */
	if (!(address & SBL(3))) {
		asm("REV16 %1,%0"
				: "=r" (receive)
				: "r" (receive));
	}
	L3GD20_SelectChipHigh();
	return receive;
}

void L3GD20_WriteReg16(uint8_t address, uint16_t data) {
	uint8_t dat[] = { address | MULTIPLE_WRITE, (uint8_t) (data >> 8),
			(uint8_t) data };
	L3GD20_SelectChipLow();
	/**
	 * Change mode into 2 full-duplex (2 lines needed)
	 */
	__SPI_DIRECTION_2LINES(&hspi2);
	HAL_SPI_Transmit(&hspi2, dat, 3, 10);
	L3GD20_SelectChipHigh();
}

void L3GD20_ReadRotation(L3GD20_Rotation_t* rotation) {
	/**
	 * Wait for new data in register
	 */
	while (!(L3GD20_ReadReg8(STATUS_REG) & ZYXDA)) {
	}
	/**
	 * Read 16-bits data from gyroscope
	 * Input for L3GD20_ReadReg16 is LSB register with value
	 */
	rotation->xAxisRotation = L3GD20_ReadReg16(OUT_X_L);
	rotation->yAxisRotation = L3GD20_ReadReg16(OUT_Y_L);
	rotation->zAxisRotation = L3GD20_ReadReg16(OUT_Z_L);
}
/**
 * @}
 */
