/*
 * lsm303.c
 *
 *  Created on: 30.04.2018
 *      Author: Piotr Gogola
 */

/**
 * \file
 * \brief Definition of functions to
 * communicate with LSM303C accelerometer
 */

#include "../lsm303c/lsm303c.h"

#include "main.h"
/** @defgroup LSM303C LSM303C
 * @{
 */

/**
 * \brief Redeclaration of SPI2
 * Communication is realized with SPI2 in half-duplex mode
 */
extern SPI_HandleTypeDef hspi2;

/** @defgroup LSM303C_Constants LSM303C Constants
 * @{
 */
#define WHO_AM_I_A		0x0f

#define ACT_THS_A		0x1e
#define ACT_DUR_A		0x1f

#define CTRL_REG1_A		0x20
#define CTRL_REG2_A		0x21
#define CTRL_REG3_A		0x22
#define CTRL_REG4_A		0x23
#define CTRL_REG5_A		0x24
#define CTRL_REG6_A		0x25
#define CTRL_REG7_A		0x26

#define STATUS_REG_A	0x27

#define OUT_X_L_A		0x28
#define OUT_X_H_A		0x29
#define OUT_Y_L_A		0x2a
#define OUT_Y_H_A		0x2b
#define OUT_Z_L_A		0x2c
#define OUT_Z_H_A		0x2d

#define FIFO_CTRL		0x2e
#define FIFO_SRC		0x2f
#define IG_CFG1_A		0x30
#define IG_SRC1_A		0x31
#define IG_THS_X1_A		0x32
#define IG_THS_Y1_A		0x33
#define IG_THS_Z1_A		0x34
#define IG_DUR1_A		0x35
#define IG_CFG2_A		0x36
#define IG_SRC2_A		0x37
#define IG_THS2_A		0x38
#define IG_DUR2_A		0x39

#define XL_REFERENCE	0x3a
#define XH_REFERENCE	0x3b
#define YL_REFERENCE	0x3c
#define YH_REFERENCE	0x3d
#define ZL_REFERENCE	0x3e
#define ZH_REFERENCE	0x3f

#define SINGLE_WRITE 	0x00
#define SINGLE_READ		0x80

/**
 * @}
 */

/** @defgroup LSM303C_Macros LSM303C Macros
 * @{
 */
#define SBL(x)	(1<<x)
/**
 * @}
 */

/** @addtogroup LSM303C_Constants
 * @{
 */
/**
 * ACT_THS_A
 */
#define THS6	SBL(6)
#define THS5 	SBL(5)
#define THS4 	SBL(4)
#define THS3 	SBL(3)
#define THS2 	SBL(2)
#define THS1 	SBL(1)
#define THS0	SBL(0)

/**
 * ACT_DUR_A
 */
#define DUR7 	SBL(7)
#define DUR6 	SBL(6)
#define DUR5 	SBL(5)
#define DUR4 	SBL(4)
#define DUR3 	SBL(3)
#define DUR2 	SBL(2)
#define DUR1 	SBL(1)
#define DUR0	SBL(0)

/**
 * CTRL_REG1_A
 */
#define HR		SBL(7)
#define ODR2 	SBL(6)
#define ODR1 	SBL(5)
#define ODR0 	SBL(4)
#define BDU 	SBL(3)
#define ZEN 	SBL(2)
#define YEN 	SBL(1)
#define XEN		SBL(0)

#define FIFO_EN 		SBL(7)
#define STOP_FTH		SBL(6)
#define INT_XL_INACT	SBL(5)
#define INT_XL_IG2		SBL(4)
#define INT_XL_IG1		SBL(3)
#define INT_XL_OVR		SBL(2)
#define INT_XL_FTH		SBL(1)
#define INT_XL_DRDY		SBL(0)

/**
 * CTRL_REG4_A
 */
#define BW2				SBL(7)
#define BW1				SBL(6)
#define FS1				SBL(5)
#define FS0				SBL(4)
#define BW_SCALE_ODR	SBL(3)
#define IF_ADD_INC		SBL(2)
#define I2C_DISABLE		SBL(1)
#define SIM				SBL(0)

/**
 * CTRL_REG5_A
 */
#define DEBUG 		SBL(7)
#define SOFT_RESET	SBL(6)
#define DEC1 		SBL(5)
#define DEC0 		SBL(4)
#define ST2 		SBL(3)
#define ST1			SBL(2)
#define H_LACTIVE	SBL(1)
#define PP_OD		SBL(0)

/**
 * STATUS_REG_A
 */
#define ZYXOR 	SBL(7)
#define ZOR 	SBL(6)
#define YOR 	SBL(5)
#define XOR 	SBL(4)
#define ZYXDA 	SBL(3)
#define ZDA 	SBL(2)
#define YDA 	SBL(1)
#define XDA		SBL(0)

/**
 * @}
 */

void LSM303C_Init() {
	/**
	 * Disable transmission
	 */
	LSM303C_SelectChipHigh();
	HAL_Delay(100);


	/**
	 * Soft reset
	 */
	LSM303C_WriteReg8(CTRL_REG5_A, SOFT_RESET);

	/**
	 * Enable interrupt when pin is low
	 */
	LSM303C_WriteReg8(CTRL_REG5_A, H_LACTIVE);

	/**
	 * Data ready signal interrupt on INT_XL pin
	 */
	LSM303C_WriteReg8(CTRL_REG3_A, INT_XL_DRDY);

	/**
	 * Automatically increment register address when multiple access with a serial port - IF_ADD_INC
	 * Disable I2C	-	I2C_DISABLE
	 * SPI read and write operations enabled 	- 	SIM
	 */
	LSM303C_WriteReg8(CTRL_REG4_A, IF_ADD_INC | I2C_DISABLE | SIM);

	/**
	 * Output data rate (enable sensor)  = 100Hz 	- ODR0 and ODR1
	 * Measure acceleration along X, Y and Z axis	-	XEN, YEN, ZEN
	 * Output register not updated until MSB and LSB are read	- BDU
	 */
	LSM303C_WriteReg8(CTRL_REG1_A, ODR1 | ODR0 | XEN | YEN | ZEN | BDU);
	HAL_Delay(100);
}

void LSM303C_SelectChipHigh() {
	HAL_GPIO_WritePin(MEMS_ACC_CS_GPIO_Port, MEMS_ACC_CS_Pin, GPIO_PIN_SET);
}

void LSM303C_SelectChipLow() {
	HAL_GPIO_WritePin(MEMS_ACC_CS_GPIO_Port, MEMS_ACC_CS_Pin, GPIO_PIN_RESET);
}

uint8_t LSM303C_ReadReg8(uint8_t address) {
	LSM303C_SelectChipLow();
	uint8_t receive = 0;
	address |= SINGLE_READ;
	/**
	 * Change SPI into half-duplex transmit mode
	 */
	__SPI_DIRECTION_1LINE_TX(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
	/**
	 * Change SPI into half-duplex receive mode
	 */
	__SPI_DIRECTION_1LINE_RX(&hspi2);
	HAL_SPI_Receive(&hspi2, &receive, 1, 10);
	LSM303C_SelectChipHigh();
	return receive;
}

void LSM303C_WriteReg8(uint8_t address, uint8_t data) {
	LSM303C_SelectChipLow();
	address |= SINGLE_WRITE;
	/**
	 * Change SPI into half-duplex transmit mode
	 */
	__SPI_DIRECTION_1LINE_TX(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
	/**
	 * Change SPI into half-duplex transmit mode
	 */
	__SPI_DIRECTION_1LINE_TX(&hspi2);
	HAL_SPI_Transmit(&hspi2, &data, 1, 10);
	LSM303C_SelectChipHigh();
}

uint16_t LSM303C_ReadReg16(uint8_t address) {
	uint16_t receive = 0;
	LSM303C_SelectChipLow();
	address |= SINGLE_READ;
	/**
	 * Change SPI into half-duplex transmit mode
	 */
	__SPI_DIRECTION_1LINE_TX(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
	/**
	 * Change SPI into half-duplex receive mode
	 */
	__SPI_DIRECTION_1LINE_RX(&hspi2);
	HAL_SPI_Receive(&hspi2, (uint8_t*) &receive, 2, 10);
	LSM303C_SelectChipHigh();
	return receive;
}

void LSM303C_ReadAcceleration(LSM303_Acceleration_t* acc) {
	/**
	 * Wait until data in register in accelerometer are updated
	 */
	while (!(LSM303C_ReadReg8(STATUS_REG_A) & ZYXDA)) {
	}
	/**
	 * Read values from registers
	 */
	acc->xAxisAcceleration = LSM303C_ReadReg16(OUT_X_L_A);
	acc->yAxisAcceleration = LSM303C_ReadReg16(OUT_Y_L_A);
	acc->zAxisAcceleration = LSM303C_ReadReg16(OUT_Z_L_A);
}

/**
 * @}
 */
