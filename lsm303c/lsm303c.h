/*
 * lsm303.h
 *
 *  Created on: 30.04.2018
 *      Author: Piotr Gogola
 */
/**
 * \file
 * \brief LSM303C accelerometer driver
 * Driver is written for STM32L476VGT6 microcontroller with HAL library.
 * Communication is realized with SPI 3-wire (half-duplex).
 */



#ifndef LSM303C_H_
#define LSM303C_H_

#include <stdint.h>
#include "../stm32l4xx_hal.h"

/** @addtogroup LSM303C
  * @{
  */

/**
 * \struct LSM303_Acceleration_t
 * \brief Structure which holds last read acceleration values
 *  along 3 axis: X, Y, Z
 */
typedef struct {
	int16_t xAxisAcceleration; //!< Acceleration along accelerometer local x axis
	int16_t yAxisAcceleration; //!< Acceleration along accelerometer local y axis
	int16_t zAxisAcceleration; //!< Acceleration along accelerometer local z axis
} LSM303_Acceleration_t;

/**
 * \brief Initialize LSM303C accelerometer
 * Set registers value
 */
void LSM303C_Init();

/**
 * \brief Set LSM303C chip select pin into high state
 */
void LSM303C_SelectChipHigh();

/**
 * \brief Set LSM303C chip select pin into low state
 */
void LSM303C_SelectChipLow();

/**
 * \brief Read LSM303 8-bit register
 * \param address register address to read value from
 * \return 8-bit value from register with address given by parameter
 */
uint8_t LSM303C_ReadReg8(uint8_t address);

/**
 * \brief Write 8-bit register
 * \param address 	register address to write value in
 * \param data 		data which will be written into register with address given by address parameter
 */
void LSM303C_WriteReg8(uint8_t address, uint8_t data);

/**
 * \brief Read 16-bit value saved in 2 8-bit registers
 * \param address lower register address to read value from
 * \return 16-bit value from registers with address given by parameter
 */
uint16_t LSM303C_ReadReg16(uint8_t address);

/**
 * \brief Read last updated acceleration raw data from accelerometer
 * \param acc pointer to LSM303_Acceleration_t structure
 */
void LSM303C_ReadAcceleration(LSM303_Acceleration_t* acc);

/**
 * @}
 */

#endif /* LSM303C_H_ */
