/*
 * audioController.h
 *
 *  Created on: 02.05.2018
 *      Author: Piotr Gogola
 */

/**
 * \file
 * \brief File with declaration of all structures and functions for
 * audio wave generator. Wave generator uses BSP functions for CS43L22 audio codec.
 * Communication with CS43L22 is realized with SAI periphery built STM32L476VGT6 microcontroller.
 */

#ifndef AUDIOCONTROLLER_H_
#define AUDIOCONTROLLER_H_

#include "STM32L476G-Discovery/stm32l476g_discovery.h"
#include "Components/Common/audio.h"
#include "Components/cs43l22/cs43l22.h"

/** @defgroup WaveGenerator Wave generator
 * @{
 */

/**
 * \brief Enum type describing wave types
 */
typedef enum {
	SINUS, 		//!< Sinus wave type
	TRIANGLE, 	//!< Triangle wave type
	SAW, 		//!< Saw wave type
	SQUARE		//!< Square wave type
} WaveType;

/**
 * \struct AudioState
 * \brief Strict which holds current wave generator state.
 */
typedef struct {
	WaveType waveType;		//!< Current wave type
	uint16_t currentSample;	//!< Current sample in period
	uint16_t maxSamples;	//!< Number of samples per period
	/*uint16_t*/float frequency;		//!< Generated sound frequency
	float amplitudeDiv;		//!< Rate of volume. Available values are between [0.0; 1.0]
	uint16_t (*waveFun)(); 	//!< Pointer to wave generator function
	uint16_t min; 			//!< minimum acceleration value along X axis got from calibration
	uint16_t max; 			//!< maximum acceleration value along X axis got from calibration
	float ratio;			//!<
} AudioState;

/** @defgroup GenerateWavesFunctions Functions which generate waves
 * @{
 */
uint16_t WG_sineWave();

uint16_t WG_triangleWave();

uint16_t WG_sawWave();

uint16_t WG_squareWave();
/**
 * @}
 */

/**
 * \brief Initialize wave generator.
 * Initialization include CS43L22 audio codec initialization
 * and beginning configuration of wave generator.
 * Configuration is saved in AudioState structure.
 */
void WG_init();

/**
 * \brief Start playing waves by CS43L22 audio codec
 */
void WG_play();

/**
 * \brief Function settings type of generated wave
 * \param waveType type of wave
 */
void WG_setWaveType(WaveType waveType);

/**
 * \brief Function changing generated wave frequency
 * \param xVal value read from accelerometer between -16384 and 16384
 * This value is casted to frequency between 48Hz - 12KHz
 */
void WG_changeFrequency(int16_t xVal);

/**
 * \brief Returns current generated wave frequency
 * \return frequency in Hz
 */
uint16_t WG_getFrequency();

/**
 * \brief Set generated wave volume
 * \param yVal value read from accelerometer between -16384 and 16384
 * This value is casted to amplitude
 */
void WG_setVolume(int16_t yVal);

/**
 * \brief Returns next sample from look up table
 * \return sample value
 */
uint16_t WG_getNextSample();

/**
 * \brief Switch to next wave type
 */
void WG_setNextWave();

/**
 * \brief Switch to previous wave type
 */
void WG_setPrevWave();

/**
 * \brief Calibrate for right frequency
 */
void WG_calibrate();

/**
 * @}
 */

/** @defgroup MovingAvg Moving average
 * @{
 */

/**
 * \struct MovingAverage
 * \brief Structure for moving average
 */
typedef struct {
	uint16_t * buff;
	uint16_t capacity;
	uint16_t currIndex;
	uint16_t size;
	uint32_t sum;
} MovingAverage;

/** @defgroup MovingAverageFunctions Moving average functions
 * @{
 */

/**
 * \brief Initialize moving average structure with buffer with capacity parameter size
 * \param ma - MovingAverage structure address
 * \param capacity - moving average buffer's capacity
 */
void MA_init(MovingAverage* ma, uint16_t capacity);

/**
 * \brief Deinitialize MovingAverage structure
 * \param ma - MovingAverage structure address to deinit
 */
void MA_deinit(MovingAverage* ma);

/**
 * \brief Function which returns average from MovingAverage.capacity last samples or less if buffer is not full
 * \param ma - MovingAverage structure address
 * \return average from samples in buffer
 */
float MA_getAverage(MovingAverage* ma);

/**
 * \brief Add next value to moving average buffer
 * \param ma - MovingAverage structure address
 * \param val - new value to add
 */
void MA_addValue(MovingAverage* ma, uint16_t val);

/**
 * @}
 */

/**
 * @}
 */

#endif /* AUDIOCONTROLLER_H_ */
