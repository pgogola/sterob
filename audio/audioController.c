/*
 * audioController.c
 *
 *  Created on: 02.05.2018
 *      Author: Piotr Gogola
 */

/**
 * \file
 * \brief Audio Controller file
 */

#include "../audio/audioController.h"

#include <math.h>
#include <stdlib.h>
#include "main.h"
#include "../lsm303c/lsm303c.h"

/** @addtogroup WaveGenerator
 * @{
 */

SAI_HandleTypeDef SaiHandle;	//!< SAI Handler
AUDIO_DrvTypeDef* audio_drv;	//!< CS43L22 Audio Codec handler

AudioState audioState;//!< Structure with describe current wave generator's state
MovingAverage movingAvg;	//!< Structure with describe moving average
extern uint8_t accelerometerInterrupt;

/**
 * \brief An array of pointers to wave functions
 */
static uint16_t (*wavesFun[])() = {WG_sineWave, WG_triangleWave,
	WG_sawWave, WG_squareWave };

/** @addtogroup WaveGeneratorFunctions
 * @{
 */

	void WG_changeFrequency(int16_t xVal) {
		/**
		 * Due to SAI configuration samples are generated with 48kHz.
		 * To control frequency we need to control number of samples inside period.
		 * If number of samples per period is big then frequency is low,
		 * if amount of samples is low then generated sound frequency is high.
		 *
		 * Controlling amount of samples in period it is possible to control sound frequency.
		 *
		 * xVal can be values between: -32,768 to 32,767
		 * Because maximum acceleration measured by accelerometer (set in accelerometer),
		 * which is the source for xVal value, is +/-2g and the base for calculated frequency
		 * is STM32 board orientation we need to measure only +/-1g.
		 * For this reason we can assume that xVal is between are values between: +/-16,384.
		 */

		/**
		 * Add 16364
		 */
		int32_t correctedXVal = (xVal + 16384) - audioState.min;
		/**
		 * Check if additional acceleration was not generated (correctedXVal < 0 or correctedXVal>32767)
		 * If it is, then set correctedXVal = 0 or correctedXVal = 32767
		 */
		if (correctedXVal < 0) {
			correctedXVal = 0;
		} else if (correctedXVal > 32767) {
			correctedXVal = 32767;
		}
		MA_addValue(&movingAvg, correctedXVal);

		/**
		 * Here we have values between 0 to 32767. As it was said we want frequencies from 50 to 12kHz.
		 */
		audioState.frequency = MA_getAverage(&movingAvg) * audioState.ratio
				+ 50;
		if (audioState.frequency < 1) {
			audioState.frequency = 1;
		} else if (audioState.frequency > 12000) {
			audioState.frequency = 12000;
		}
		printf("%d\r\n", (int) audioState.frequency);

		audioState.maxSamples = 48000 / audioState.frequency;
		printf("%d\r\n", (int) audioState.maxSamples);
	}

	void WG_init() {
		/*
		 * Initialize audio driver
		 * Attach CS43L22 Struct to Audio Codec handler
		 */
		audio_drv = &cs43l22_drv;
		/**
		 * Reset and initialize Codec
		 */
		audio_drv->Reset(AUDIO_I2C_ADDRESS);
		audio_drv->Init(AUDIO_I2C_ADDRESS, OUTPUT_DEVICE_HEADPHONE, 70,
		AUDIO_FREQUENCY_48K);

		audioState.amplitudeDiv = 0.5;	//!< Init volume
		audioState.waveType = SINUS;	//!< Init wave type
		audioState.currentSample = 0;	//!< Start at the begining
		audioState.maxSamples = 1000;	//!< Init max samples
		audioState.frequency = 48;		//!< Init frequency (48Hz)
		audioState.waveFun = WG_sineWave; //!< Init wave function
		audioState.min = UINT16_MAX; 	//!< Init minimum value as available max
		audioState.max = 0;				//!< Init maximum value as avaliable min
		MA_init(&movingAvg, 20);		//!< Init moving average
	}

	void WG_play() {
		audio_drv->Play(AUDIO_I2C_ADDRESS, NULL, 50);
	}

	void WG_setWaveType(WaveType waveType) {
		audioState.waveType = waveType;
	}

	uint16_t WG_getNextSample() {
		/**
		 * Increment current sample counter and check if index is correct
		 */
		audioState.currentSample++;
		if (audioState.currentSample >= audioState.maxSamples) {
			audioState.currentSample = 0;
		}

		return audioState.waveFun();
	}

	uint16_t WG_sineWave() {
		return (sinf(
				2.0f * M_PI * audioState.currentSample / audioState.maxSamples)
				+ 1) * 2098 * audioState.amplitudeDiv;
	}

	uint16_t WG_triangleWave() {
		return (audioState.currentSample < audioState.maxSamples / 2 ?
				8192.0f * audioState.currentSample / audioState.maxSamples :
				8192.0f * (audioState.maxSamples-audioState.currentSample) / audioState.maxSamples)
				* audioState.amplitudeDiv;
	}

	uint16_t WG_sawWave() {
		return (4096.0f * audioState.currentSample / audioState.maxSamples)
				* audioState.amplitudeDiv;
	}

	uint16_t WG_squareWave() {
		return audioState.currentSample < audioState.maxSamples / 2 ?
				0 : 4095 * audioState.amplitudeDiv;
	}

	uint16_t WG_getFrequency() {
		return audioState.frequency;
	}

	void WG_setVolume(int16_t yVal) {
		audioState.amplitudeDiv = (2.0 * yVal + INT16_MAX) / UINT16_MAX;
	}

	void WG_setNextWave() {
		audioState.waveType = (audioState.waveType + 1) % 4;
		audioState.waveFun = wavesFun[audioState.waveType];
		printf("setNextValue %d\r\n", audioState.waveType);
	}

	void WG_setPrevWave() {
		if (audioState.waveType == 0)
			audioState.waveType = 3;
		else {
			audioState.waveType--;
		}
		audioState.waveFun = wavesFun[audioState.waveType];
		printf("setPrevValue %d\r\n", audioState.waveType);
	}

	void WG_calibrate() {
		printf("calib\r\n");
		LSM303_Acceleration_t acceleration;

		while (!HAL_GPIO_ReadPin(JOY_CENTER_GPIO_Port, JOY_CENTER_Pin)) {

			spiChange(SPI_DIRECTION_1LINE);
			if (1 <= accelerometerInterrupt) {
				LSM303C_ReadAcceleration(&acceleration); //Read acceleration
				spiChange(SPI_DIRECTION_2LINES);
				int32_t currentX = acceleration.xAxisAcceleration + 16384;
				audioState.max =
						audioState.max > currentX ? audioState.max : currentX;
				audioState.min =
						audioState.min < currentX ? audioState.min : currentX;
				accelerometerInterrupt--;
				printf("%d\r\n", acceleration.xAxisAcceleration);
			}

		}
		audioState.ratio = 12000.0f / (audioState.max - audioState.min);
		printf("%d\r\n", acceleration.xAxisAcceleration);
	}

	/**
	 * @}
	 */
	/**
	 * @}
	 */
	/** @addtogroup MovingAvg
	 * @{
	 */

	/** @addtogroup MovingAverageFunctions
	 * @{
	 */
	void MA_init(MovingAverage* ma, uint16_t capacity) {
		ma->buff = (uint16_t*) malloc(2 * capacity);
		ma->capacity = capacity;
		ma->currIndex = 0;
		ma->size = 0;
		ma->sum = 0;
	}

	void MA_deinit(MovingAverage* ma) {
		free(ma->buff);
		ma->buff = NULL;
		ma->capacity = 0;
		ma->currIndex = 0;
		ma->size = 0;
		ma->sum = 0;
	}

	float MA_getAverage(MovingAverage* ma) {
		return (float) ma->sum / ma->size;
	}

	void MA_addValue(MovingAverage* ma, uint16_t val) {
		ma->sum += val;
		if (ma->size == ma->capacity) {
			ma->sum -= ma->buff[ma->currIndex];
		} else {
			ma->size++;
		}
		ma->buff[ma->currIndex] = val;
		ma->currIndex = (ma->currIndex + 1) % ma->capacity;
	}

	/**
	 * @}
	 */
	/**
	 * @}
	 */
